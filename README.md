# Setup instructions
App build and sample configuration files can be found in [Downloads](https://bitbucket.org/appcardinc/android-marketing-app/downloads/).

On first launch skip all the onboarding steps. 
Don't connect device to the Wi-Fi!

## 1. App install and config
1. [Set up a device](https://developer.android.com/studio/run/device#setting-up) for app install.
2. Install [adb](https://developer.android.com/studio/command-line/adb), if needed.
3. Push the configuration
	* `adb push config.txt sdcard`
	* `adb push video/* sdcard`
4. Install the app
	* `adb install marketing20220607.apk`

## 2. Enable the kiosk mode
6. Press Back, go to Settings
	* Apps&notifications -> Advanced -> Default apps -> Home app -> Appcard
	* Apps&notifications -> Advanced -> Default apps -> SMS app -> Appcard
7. Run the command
	* `adb shell dpm set-device-owner com.appcard.marketing/.receiver.AdminReceiver`

## 3. Clean and run
8. Disable Developer options   
   Settings -> System -> Developer options -> Off
9. Restart the device


## Updating the installed app
Run the command `adb install -r apkName`  
For example, `adb install -r marketing20220607.apk`

## Uninstall the app
Run the following commands:

1. `adb shell am broadcast -a com.appcard.marketing.uninstall`
2. `adb uninstall com.appcard.marketing`
