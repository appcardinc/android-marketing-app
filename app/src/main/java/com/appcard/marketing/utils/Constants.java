package com.appcard.marketing.utils;

import android.os.Environment;

import java.io.File;
import java.io.FilePermission;

public class Constants {
    public static String FILE_NAME = "config.txt";
    public static String FILE_PATH = "";
    public static String FILE = "/" + Constants.FILE_NAME;

    public static String INIT_FILE_PATH = Environment.getExternalStorageDirectory().toString();
    public static String INIT_FILE = Constants.INIT_FILE_PATH + "/" + Constants.FILE_NAME;

    /**
     * main video
     */
    public static String VIDEO_URL = "";

    /**
     * coupons video url
     */
    public static String VIDEO_COUPONS_URL = "";
    public static String VIDEO_MORE_URL1 = "";
    public static String VIDEO_MORE_URL2 = "";
    public static String VIDEO_MORE_URL3 = "";
    public static String PERSON_PHONE = "";
    public static String PERSON_NAME = "";
    public static String MERCHANT_NAME = "";
    public static String NOTIFICATIONS_PHONE = "";
    public static String GREETING_NAME = "";

    /**
     * title
     */
    public static String MAIN_TITLE = "";
    public static String SECONDARY_TITLE = "";
    public static String MORE_TITLE = "";
    /**
     * video url action
     */
    public static String ACTION = "url";
    public static String BOOT = "boot";
    public static String WAKE = "wake";
    public static String MENU_LOAD = "menuload";
    public static String GREETING = "Hi %s!";
    public static String BUTTON = "button [%s]";
    public static String CALL_NAME = "Call %s";
    public static String SMS_NAME = "SMS %s";
    public static String MORE1 = "more1";
    public static String MORE2 = "more2";
    public static String MORE3 = "more3";
    public static boolean IS_FIRST = false;
    public static int CALL_STATUS = -1;
    public static String REMOVE_BROADCAST = "com.appcard.marketing.uninstall";
    public static String SHUTDOWN_BROADCAST = "android.intent.action.ACTION_SHUTDOWN";
    public static String BOOT_BROADCAST = "com.appcard.marketing.BOOT_COMPLETED";
    public static String CALL_BROADCAST = "com.appcard.marketing.CALL_PHONE";
    public static boolean IS_WAKE = false;
    public static boolean IS_BOOT = true;
}
