package com.appcard.marketing.utils;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.appcard.marketing.sms.SmsReadDao;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Utils {
    public static String TAG = "Utils";
    public static Utils mUtils;
    private static final int FAST_CLICK_DELAY_TIME = 1000;
    private static long lastClickTime;

    public static Utils getInstance() {
        if (mUtils == null) {
            mUtils = new Utils();
        }
        return mUtils;
    }

    public static boolean isFastClick() {
        boolean flag = true;
        long currentClickTime = System.currentTimeMillis();
        if ((currentClickTime - lastClickTime) >= FAST_CLICK_DELAY_TIME) {
            flag = false;
        }
        lastClickTime = currentClickTime;
        return flag;
    }

    public void sendNotifications(Context context, String message) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "sendNotifications " + message);
            String msg = Constants.MERCHANT_NAME + " : " + message;
            SmsReadDao.sendMessage(msg, context, Constants.NOTIFICATIONS_PHONE);
            SmsReadDao.deleteLastOneSendedSms(context);
        }
    }

    public void getInfo(Context context) {
        String fileString = null;
        Constants.FILE_PATH = context.getFilesDir().getPath();
        Constants.FILE = Constants.FILE_PATH + Constants.FILE;
        try {
            if (new File(Constants.INIT_FILE).exists()) {
                fileString = readExternal(Constants.INIT_FILE);
                Log.d(TAG, fileString);
                saveToSDCard(Constants.FILE_PATH, fileString);
                new File(Constants.INIT_FILE).delete();
            } else if (new File(Constants.FILE).exists()) {
                fileString = readExternal(Constants.FILE);
                Log.d(TAG, fileString);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!TextUtils.isEmpty(fileString)) {
            Gson gson = new Gson();
            try {
                ConfigBean configBean = (ConfigBean) gson.fromJson(fileString, ConfigBean.class);
                ConfigBean.Video video = configBean.getVideo();
                Constants.VIDEO_URL = Constants.INIT_FILE_PATH + "/" + video.getMain();
                Constants.VIDEO_COUPONS_URL = Constants.INIT_FILE_PATH + "/" + video.getSecondary();
                Constants.VIDEO_MORE_URL1 = Constants.INIT_FILE_PATH + "/" + video.getMore1();
                Constants.VIDEO_MORE_URL2 = Constants.INIT_FILE_PATH + "/" + video.getMore2();
                Constants.VIDEO_MORE_URL3 = Constants.INIT_FILE_PATH + "/" + video.getMore3();
                Constants.GREETING_NAME = configBean.getMerchant_person_name();
                Constants.MAIN_TITLE = configBean.getMain_title();
                Constants.SECONDARY_TITLE = configBean.getSecondary_title();
                Constants.MORE_TITLE = configBean.getMore_title();
                Constants.PERSON_PHONE = configBean.getSales_person_phone();
                Constants.PERSON_NAME = configBean.getSales_person_name();
                Constants.MERCHANT_NAME = configBean.getMerchant_name();
                Constants.NOTIFICATIONS_PHONE = configBean.getNotifications_phone();
                Log.d(TAG, new Gson().toJson(configBean));
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }
        }
    }

    public void saveToSDCard(String path, String content) {
        if (!new File(path).exists()) {
            new File(path).mkdirs();
        }
        File file = new File(Constants.FILE);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            OutputStream out = new FileOutputStream(file);
            out.write(content.getBytes());
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static String readExternal(String filename) throws IOException {
        Log.d(TAG, "readExternal" + filename);
        StringBuilder sb = new StringBuilder("");
        FileInputStream inputStream = new FileInputStream(filename);

        byte[] buffer = new byte[1024];
        int len = inputStream.read(buffer);
        while (len > 0) {
            sb.append(new String(buffer, 0, len));
            len = inputStream.read(buffer);
        }
        inputStream.close();
        return sb.toString();
    }

    public String stringFormat(String str) {
        if (!TextUtils.isEmpty(str)) {
            return str;
        } else return "";
    }

    public static void saveBroadCastName(Context context, String name) {
        SharedPreferences.Editor editor = context.getSharedPreferences("broadcast", Context.MODE_PRIVATE).edit();
        editor.putString("name", name);
        editor.commit();
    }

    public static String getBroadCastName(Context context) {
        SharedPreferences broadcast = context.getSharedPreferences("broadcast", Context.MODE_PRIVATE);
        return broadcast.getString("name", "");
    }
}
