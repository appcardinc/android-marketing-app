package com.appcard.marketing.utils;

public class ConfigBean {

    private Video video;
    private String main_title;
    private String secondary_title;
    private String more_title;
    private String merchant_name;
    private String sales_person_name;
    private String sales_person_phone;
    private String notifications_phone;
    private String merchant_person_name;

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public String getMain_title() {
        return main_title;
    }

    public void setMain_title(String main_title) {
        this.main_title = main_title;
    }

    public String getSecondary_title() {
        return secondary_title;
    }

    public void setSecondary_title(String secondary_title) {
        this.secondary_title = secondary_title;
    }

    public String getMore_title() {
        return more_title;
    }

    public void setMore_title(String more_title) {
        this.more_title = more_title;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }

    public String getSales_person_name() {
        return sales_person_name;
    }

    public void setSales_person_name(String sales_person_name) {
        this.sales_person_name = sales_person_name;
    }

    public String getSales_person_phone() {
        return sales_person_phone;
    }

    public void setSales_person_phone(String sales_person_phone) {
        this.sales_person_phone = sales_person_phone;
    }

    public String getNotifications_phone() {
        return notifications_phone;
    }

    public void setNotifications_phone(String notifications_phone) {
        this.notifications_phone = notifications_phone;
    }

    public String getMerchant_person_name() {
        return merchant_person_name;
    }

    public void setMerchant_person_name(String merchant_person_name) {
        this.merchant_person_name = merchant_person_name;
    }


    public class Video {
        private String main;
        private String secondary;
        private String more1;
        private String more2;
        private String more3;

        public String getMain() {
            return main;
        }

        public void setMain(String main) {
            this.main = main;
        }

        public String getSecondary() {
            return secondary;
        }

        public void setSecondary(String secondary) {
            this.secondary = secondary;
        }

        public String getMore1() {
            return more1;
        }

        public void setMore1(String more1) {
            this.more1 = more1;
        }

        public String getMore2() {
            return more2;
        }

        public void setMore2(String more2) {
            this.more2 = more2;
        }

        public String getMore3() {
            return more3;
        }

        public void setMore3(String more3) {
            this.more3 = more3;
        }
    }

}
