package com.appcard.marketing.utils;

public class UninstallBean {
    private String broadcast;

    public String getBroadcast() {
        return broadcast;
    }

    public void setBroadcast(String broadcast) {
        this.broadcast = broadcast;
    }
}
