package com.appcard.marketing.sms;

public class SmsInfo {

    public SmsInfo(String id, String address, String date, String date_sent, String read, String status, String type, String body) {
        this.id = id;
        this.address = address;
        this.date = date;
        this.read = read;
        this.status = status;
        this.type = type;
        this.body = body;
        this.date_sent = date_sent;
    }

    private String id;
    private String address;
    private String date;
    private String date_sent;
    private String read;
    private String status;
    private String type;
    private String body;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate_sent() {
        return date_sent;
    }

    public void setDate_sent(String date_sent) {
        this.date_sent = date_sent;
    }

    public String getRead() {
        return read;
    }

    public void setRead(String read) {
        this.read = read;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
