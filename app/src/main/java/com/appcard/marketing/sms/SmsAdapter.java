package com.appcard.marketing.sms;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appcard.marketing.R;

import java.util.List;

public class SmsAdapter extends BaseAdapter {
    public List<SmsBean> list;

    public SmsAdapter(List<SmsBean> list) {
        this.list = list;

    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.sms_item, null);
        TextView content = convertView.findViewById(R.id.sms_item);
        SmsBean s = list.get(position);
        content.setText(s.getContent());
        //  *  type：1receive，2send
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) content.getLayoutParams();
        if (s.getType().equals("1")) {
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            content.setBackground(parent.getContext().getResources().getDrawable(R.drawable.send_bg));
            content.setTextColor(parent.getContext().getResources().getColor(R.color.black));
        } else {
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            content.setTextColor(parent.getContext().getResources().getColor(R.color.white));
            content.setBackground(parent.getContext().getResources().getDrawable(R.drawable.green_bg));
        }
        return convertView;
    }
}
