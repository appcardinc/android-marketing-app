package com.appcard.marketing.sms;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;

import com.appcard.marketing.utils.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class SmsReadDao {
    public static String TAG = SmsReadDao.class.getName();

    public static void sendMessage(String content, Context context, String phone) {
        if (TextUtils.isEmpty(phone)) {
            return;
        }
        SmsManager sm = SmsManager.getDefault();
        if (content.length() > 70) {
            List<String> contents = sm.divideMessage(content);
            for (String sms : contents) {
                sm.sendTextMessage(phone, null, sms, null, null);//发送短信
            }
        } else {
            Log.d(TAG, phone);
            sm.sendTextMessage(phone, null, content, null, null);//发送短信
        }
        addMessage(context, phone, content, "2");
    }

    public static void addMessage(Context context, String phone, String content, String type) {
        ContentValues values = new ContentValues();
        values.put("address", phone);
        values.put("body", content);
        values.put("date", System.currentTimeMillis());
        values.put("type", type);
        context.getContentResolver().insert(Uri.parse("content://sms"), values);
    }

    public static List<SmsBean> obtainPhoneMessage(Context context, String phone) {
        List<SmsBean> list = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String[] projection = new String[]{"_id", "address", "person", "body", "date", "type"};
        Cursor cur = cr.query(Uri.parse("content://sms/"), projection, null, null, "date desc");
        if (null == cur) {
            return list;
        }
        while (cur.moveToNext()) {
            String number = cur.getString(cur.getColumnIndex("address"));//phone
            String name = cur.getString(cur.getColumnIndex("person"));//name
            String body = cur.getString(cur.getColumnIndex("body"));
            String type = cur.getString(cur.getColumnIndex("type"));
            String id = cur.getString(cur.getColumnIndex("_id"));
            if (number.equals(phone)) {
                SmsBean smsBean = new SmsBean();
                smsBean.setContent(body);
                smsBean.setType(type);
                smsBean.setId(id);
                list.add(smsBean);
            }
        }
        Log.d(TAG, "" + list.size());
        Collections.reverse(list);
        return list;

    }

    /**
     * type =>  1 receive，2 send
     *
     * @param context
     * @return
     */
    public static SmsInfo getLastSendedSmsInfo(Context context) {
        Log.d(TAG, "enter getSmsChangedInfo method");
        String[] projection = new String[]{"_id", "address", "date",
                "date_sent", "read", "status", "type", "body"};
        Cursor cursor = context.getContentResolver().query(
                Uri.parse("content://sms/"), projection, "type==2",
                null, "_id desc limit 1");
        SmsInfo smsInfo = null;
        try {
            Log.d(TAG, "count-- " + cursor.getCount());
            while (cursor != null && !cursor.isClosed() && cursor.moveToNext()) {
                String id = cursor.getString(cursor
                        .getColumnIndexOrThrow("_id"));
                String address = cursor.getString(cursor
                        .getColumnIndexOrThrow("address"));
                String date = cursor.getString(cursor
                        .getColumnIndexOrThrow("date"));
                String date_sent = cursor.getString(cursor
                        .getColumnIndexOrThrow("date_sent"));
                String read = cursor.getString(cursor
                        .getColumnIndexOrThrow("read"));
                String status = cursor.getString(cursor
                        .getColumnIndexOrThrow("status"));
                String type = cursor.getString(cursor
                        .getColumnIndexOrThrow("type"));
                String body = cursor.getString(cursor
                        .getColumnIndexOrThrow("body"));

                smsInfo = new SmsInfo(id, address, date, date_sent,
                        read, status, type, body);
                Log.e(TAG, smsInfo.toString());
            }
        } finally {
            if (cursor != null)
                cursor.close();
            return smsInfo;
        }
    }

    // type =>  1receive，2 send，5sendfail
    public static int deleteOneSendSms(Context context, String id) {
        Uri contentUri = Uri.parse("content://sms");
        // SELECT * FROM Persons WHERE (FirstName='Thomas' OR
        // FirstName='William') AND LastName='Carter'
        int delete = context.getContentResolver().delete(contentUri,
                "type==2 and _id=?", new String[]{id});
        return delete;
    }

    /**
     * delete last sms
     *
     * @param context
     * @return
     */
    public static final int deleteLastOneSendedSms(Context context) {
        SmsInfo info = getLastSendedSmsInfo(context);
        if (info != null && !TextUtils.isEmpty(info.getId())) {
            int delete = deleteOneSendSms(context, info.getId());
            return delete;
        } else return -1;

    }
}