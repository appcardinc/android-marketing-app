package com.appcard.marketing.sms;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.appcard.marketing.utils.Constants;

import org.greenrobot.eventbus.EventBus;


public class SmsReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.provider.Telephony.SMS_DELIVER")) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    SmsMessage[] messages = getMessagesFromIntent(intent);
                    for (SmsMessage message : messages) {
                        if (Constants.PERSON_PHONE.equals(message.getOriginatingAddress())) {
                            SmsReadDao.addMessage(context, message.getOriginatingAddress(), message.getDisplayMessageBody(), "1");
                            EventBus.getDefault().post(message);
                        }
                    }
                    Log.d("SmsActivity", "SmsReceiver onReceive: ");
                    break;
                case Activity.RESULT_CANCELED:
                    break;
            }
        }
    }

    public final SmsMessage[] getMessagesFromIntent(Intent intent) {
        Object[] messages = (Object[]) intent.getSerializableExtra("pdus");
        byte[][] pduObjs = new byte[messages.length][];
        for (int i = 0; i < messages.length; i++) {
            pduObjs[i] = (byte[]) messages[i];
        }
        byte[][] pdus = new byte[pduObjs.length][];
        int pduCount = pdus.length;
        SmsMessage[] msgs = new SmsMessage[pduCount];
        for (int i = 0; i < pduCount; i++) {
            pdus[i] = pduObjs[i];
            msgs[i] = SmsMessage.createFromPdu(pdus[i]);
        }
        return msgs;
    }
}