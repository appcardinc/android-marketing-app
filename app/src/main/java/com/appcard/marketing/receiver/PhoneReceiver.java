package com.appcard.marketing.receiver;


import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.appcard.marketing.utils.Constants;
import com.appcard.marketing.utils.Utils;

public class PhoneReceiver extends BroadcastReceiver {
    public static String TAG = PhoneReceiver.class.getSimpleName();
    public Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            String phoneNumber = intent
                    .getStringExtra(Intent.EXTRA_PHONE_NUMBER);
            Log.d(TAG, "call OUT:" + phoneNumber);
        } else if (Constants.CALL_BROADCAST.equals(intent.getAction())) {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Service.TELEPHONY_SERVICE);
            tm.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);
        }
    }

    PhoneStateListener listener = new PhoneStateListener() {

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);
            switch (state) {
                case TelephonyManager.CALL_STATE_IDLE:
                    //mul IDIE status
                    if (TelephonyManager.CALL_STATE_OFFHOOK == Constants.CALL_STATUS
                            || TelephonyManager.CALL_STATE_RINGING == Constants.CALL_STATUS) {
                        Constants.CALL_STATUS = TelephonyManager.CALL_STATE_IDLE;
                        Utils.getInstance().sendNotifications(context, Constants.MENU_LOAD);
                    }
                    Log.d(TAG, "CALL_STATE_IDLE");
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Constants.CALL_STATUS = TelephonyManager.CALL_STATE_OFFHOOK;
                    Log.d(TAG, "CALL_STATE_OFFHOOK");
                    break;
                case TelephonyManager.CALL_STATE_RINGING:
                    Constants.CALL_STATUS = TelephonyManager.CALL_STATE_RINGING;
                    Log.d(TAG, "tel:phone" + incomingNumber);
                    break;
            }
        }
    };
}