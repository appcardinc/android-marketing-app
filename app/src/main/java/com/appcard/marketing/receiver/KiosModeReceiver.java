package com.appcard.marketing.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.appcard.marketing.MainActivity;
import com.appcard.marketing.utils.Constants;
import com.appcard.marketing.utils.UninstallBean;
import com.appcard.marketing.utils.Utils;

import org.greenrobot.eventbus.EventBus;

public class KiosModeReceiver extends BroadcastReceiver {
    private String TAG = MainActivity.class.getSimpleName();


    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d("KiosModeReceiver", action);
        if (intent.getAction().equals(Constants.BOOT_BROADCAST)) {
            Utils.saveBroadCastName(context, Constants.BOOT_BROADCAST);
            Utils.getInstance().sendNotifications(context, Constants.BOOT);
        }
        if (intent.getAction().equals(Constants.REMOVE_BROADCAST)) {
            Log.d(TAG, Constants.REMOVE_BROADCAST);
            UninstallBean uninstallBean = new UninstallBean();
            uninstallBean.setBroadcast(Constants.REMOVE_BROADCAST);
            EventBus.getDefault().post(uninstallBean);
        }
        if (intent.getAction().equals(Constants.SHUTDOWN_BROADCAST)) {
            Utils.saveBroadCastName(context, Constants.SHUTDOWN_BROADCAST);
        }
        if (action.equals(Intent.ACTION_SCREEN_OFF)) {
            Constants.IS_WAKE = true;
        } else if (action.equals(Intent.ACTION_SCREEN_ON)) {
            if (Constants.IS_WAKE) {
                Constants.IS_WAKE = false;
                Utils.getInstance().sendNotifications(context, Constants.WAKE);
            }
        }
    }
}
