package com.appcard.marketing;

import android.Manifest;
import android.app.Application;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;

import androidx.core.app.ActivityCompat;

import com.appcard.marketing.receiver.KiosModeReceiver;
import com.appcard.marketing.receiver.PhoneReceiver;
import com.appcard.marketing.utils.Constants;
import com.appcard.marketing.utils.Utils;

public class InitApplication extends Application {
    public static String TAG = InitApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        Constants.IS_FIRST = true;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                if (Environment.isExternalStorageManager()) {
                    Utils.getInstance().getInfo(this);
                }
            } else {
                Utils.getInstance().getInfo(this);
            }
        }
        IntentFilter intentFilterButton = new IntentFilter();
        intentFilterButton.addAction(Constants.REMOVE_BROADCAST);
        intentFilterButton.addAction(Constants.SHUTDOWN_BROADCAST);
        intentFilterButton.addAction(Constants.BOOT_BROADCAST);
        intentFilterButton.addAction(Intent.ACTION_SCREEN_OFF);
        intentFilterButton.addAction(Intent.ACTION_SCREEN_ON);
        intentFilterButton.addAction(getPackageName());
        KiosModeReceiver kiosModeReceiver = new KiosModeReceiver();
        registerReceiver(kiosModeReceiver, intentFilterButton);

        IntentFilter phoneFilter = new IntentFilter();
        phoneFilter.addAction(Intent.ACTION_NEW_OUTGOING_CALL);
        phoneFilter.addAction(Constants.CALL_BROADCAST);
        phoneFilter.addAction(getPackageName());
        PhoneReceiver phoneReceiver = new PhoneReceiver();
        registerReceiver(phoneReceiver, phoneFilter);

        Intent phoneIntent = new Intent();
        phoneIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        phoneIntent.setAction(Constants.CALL_BROADCAST);
        this.sendBroadcast(phoneIntent);

        String broadCastName = Utils.getBroadCastName(this);
        if (Constants.SHUTDOWN_BROADCAST.equals(broadCastName)) {
            Intent intent = new Intent();
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setAction(Constants.BOOT_BROADCAST);
            this.sendBroadcast(intent);
        }

    }
}
