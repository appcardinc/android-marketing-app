package com.appcard.marketing;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.provider.Telephony;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.internal.telephony.ITelephony;
import com.appcard.marketing.activity.BaseActivity;
import com.appcard.marketing.activity.MoreActivity;
import com.appcard.marketing.activity.PlayVideoActivity;
import com.appcard.marketing.activity.SmsActivity;
import com.appcard.marketing.utils.Constants;
import com.appcard.marketing.utils.Utils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private RelativeLayout mCall;
    private RelativeLayout mSMS;
    private RelativeLayout mVideo;
    private RelativeLayout mCoupons;
    private RelativeLayout mMore;
    private TextView mCallContent;
    private TextView mSMSContent;
    private TextView mMainTitle;
    private TextView mSecTitle;
    private TextView mMoreTitle;
    private TextView mHiTitle;
    private final static int PERMISSION_RESULT = 0x100;
    private final static int DEFAULTSMS = 0x110;
    private final static int REQUEST_CALL_PHONE = 0x112;
    private final static int REQUEST_INIT_VIDEO = 0x113;
    private final static int REQUEST_VIDEO = 0x114;
    private final static int REQUEST_SMS = 0x115;
    private final static int REQUEST_MORE_VIDEO = 0x116;
    private final static int REQUEST_API11_READ = 0x117;
    private final static int REQUEST_END_PHONE = 0x118;
    private final static String TAG = MainActivity.class.getSimpleName();
    private boolean isPause = false;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        List<String> permissionList = new ArrayList<>();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(Manifest.permission.CALL_PHONE);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(Manifest.permission.SEND_SMS);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(Manifest.permission.ANSWER_PHONE_CALLS);
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
                permissionList.add(Manifest.permission.READ_SMS);
            }
        }
        if (permissionList.size() > 0) {
            ActivityCompat.requestPermissions(this, permissionList.toArray(new String[permissionList.size()]), PERMISSION_RESULT);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                if (Environment.isExternalStorageManager()) {
                    Utils.getInstance().getInfo(this);
                    gotoInitVideo();
                } else {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, REQUEST_API11_READ);
                }
            } else {
                Utils.getInstance().getInfo(this);
                gotoInitVideo();
            }
        } else {
            init();
            defaultSms();
        }
    }

    private void init() {
        mHiTitle = findViewById(R.id.greeting);
        mCall = findViewById(R.id.call);
        mCallContent = findViewById(R.id.call_content);
        mSMS = findViewById(R.id.sms);
        mSMSContent = findViewById(R.id.sms_content);
        mVideo = findViewById(R.id.video);
        mMainTitle = findViewById(R.id.main_title);
        mCoupons = findViewById(R.id.coupons);
        mSecTitle = findViewById(R.id.second_title);
        mMore = findViewById(R.id.more);
        mMoreTitle = findViewById(R.id.more_title);
        mCall.setOnClickListener(this);
        mSMS.setOnClickListener(this);
        mVideo.setOnClickListener(this);
        mCoupons.setOnClickListener(this);
        mMore.setOnClickListener(this);
        if (!TextUtils.isEmpty(Constants.GREETING_NAME)) {
            mHiTitle.setText(String.format(Constants.GREETING, Constants.GREETING_NAME));
        }
        mCallContent.setText(String.format(Constants.CALL_NAME, Constants.PERSON_NAME));
        mSMSContent.setText(String.format(Constants.SMS_NAME, Constants.PERSON_NAME));
        mMainTitle.setText(Utils.getInstance().stringFormat(Constants.MAIN_TITLE));
        mSecTitle.setText(Utils.getInstance().stringFormat(Constants.SECONDARY_TITLE));
        mMoreTitle.setText(Utils.getInstance().stringFormat(Constants.MORE_TITLE));
    }

    private void gotoInitVideo() {
        if (Constants.IS_FIRST) {
            Constants.IS_FIRST = false;
            Intent intent = new Intent(this, PlayVideoActivity.class);
            startActivityForResult(intent, REQUEST_INIT_VIDEO);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if ((Constants.CALL_STATUS == 1 || Constants.CALL_STATUS == 2)) {
            endCall();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.call:
                if (!Utils.isFastClick()) {
                    Utils.getInstance().sendNotifications(this, String.format(Constants.BUTTON, String.format(Constants.CALL_NAME, Constants.PERSON_NAME)));
                    callPhone();
                }
                break;
            case R.id.sms:
                if (!Utils.isFastClick()) {
                    Intent intent = new Intent(this, SmsActivity.class);
                    startActivityForResult(intent, REQUEST_SMS);
                    Utils.getInstance().sendNotifications(this, String.format(Constants.BUTTON, String.format(Constants.SMS_NAME, Constants.PERSON_NAME)));
                }
                break;
            case R.id.video:
                if (!Utils.isFastClick()) {
                    Utils.getInstance().sendNotifications(this, String.format(Constants.BUTTON, Constants.MAIN_TITLE));
                    playVideo(Constants.VIDEO_URL);
                }
                break;
            case R.id.coupons:
                if (!Utils.isFastClick()) {
                    playVideo(Constants.VIDEO_COUPONS_URL);
                    Utils.getInstance().sendNotifications(this, String.format(Constants.BUTTON, Constants.SECONDARY_TITLE));
                }
                break;
            case R.id.more:
                if (!Utils.isFastClick()) {
                    Intent moreIntent = new Intent(this, MoreActivity.class);
                    startActivityForResult(moreIntent, REQUEST_MORE_VIDEO);
                    Utils.getInstance().sendNotifications(this, String.format(Constants.BUTTON, Constants.MORE_TITLE));
                }
                break;
        }
    }

    public void defaultSms() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            String myPackageName = getPackageName();
            String defaultSmsApp = Telephony.Sms.getDefaultSmsPackage(this);
            if (!myPackageName.equals(defaultSmsApp)) {
                Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
                intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, myPackageName);
                startActivity(intent);
                return;
            }
        }
    }

    public void callPhone() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            Uri parse = Uri.parse("tel:" + Constants.PERSON_PHONE);
            callIntent.setData(parse);
            startActivityForResult(callIntent, REQUEST_CALL_PHONE);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_RESULT);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPause = true;
    }

    public void playVideo(String url) {
        Intent intent = new Intent(this, PlayVideoActivity.class);
        intent.putExtra(Constants.ACTION, url);
        startActivityForResult(intent, REQUEST_VIDEO);

    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case DEFAULTSMS:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    final String myPackageName = getPackageName();
                    if (Telephony.Sms.getDefaultSmsPackage(this).equals(myPackageName)) {
                        Log.d(TAG, "DEFAULTSMS");
                    }
                }
                break;
            case PERMISSION_RESULT:
            case REQUEST_INIT_VIDEO:
                Log.d(TAG, "PERMISSION_RESULT");
                Utils.getInstance().sendNotifications(this, Constants.MENU_LOAD);
                init();
                defaultSms();
                break;
            case REQUEST_SMS:
            case REQUEST_VIDEO:
            case REQUEST_MORE_VIDEO:
                Log.d(TAG, "REQUEST");
                Utils.getInstance().sendNotifications(this, Constants.MENU_LOAD);
                break;
            case REQUEST_API11_READ:
                Utils.getInstance().getInfo(this);
                gotoInitVideo();
                break;
            case REQUEST_END_PHONE:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    TelecomManager tmAbove = (TelecomManager) getSystemService(Context.TELECOM_SERVICE);
                    tmAbove.endCall();
                }
                break;
            default:
                break;
        }
    }


    public void endCall() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            TelecomManager tmAbove = (TelecomManager) getSystemService(Context.TELECOM_SERVICE);

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ANSWER_PHONE_CALLS) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ANSWER_PHONE_CALLS}, REQUEST_END_PHONE);
            } else {
                tmAbove.endCall();
            }
        } else {
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            try {
                Class tmClazz = tm.getClass();
                Method mthEndCall = tmClazz.getDeclaredMethod("getITelephony", (Class[]) null);
                mthEndCall.setAccessible(true);
                ITelephony iTelephony = (ITelephony) mthEndCall.invoke(tm);
                iTelephony.endCall();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}