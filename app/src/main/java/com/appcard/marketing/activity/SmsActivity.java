package com.appcard.marketing.activity;

import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;

import com.appcard.marketing.sms.SmsAdapter;
import com.appcard.marketing.sms.SmsBean;
import com.appcard.marketing.sms.SmsReadDao;
import com.appcard.marketing.utils.Constants;
import com.appcard.marketing.R;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;


public class SmsActivity extends BaseActivity {
    public static String TAG = SmsActivity.class.getName();
    private EditText message;

    private List<SmsBean> list = new ArrayList<>();
    private ListView mSmsList;
    private SmsAdapter smsAdapter;
    private TextView mTitle;
    boolean isSelect = false;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            getMessageList();
            smsAdapter.notifyDataSetChanged();
            mSmsList.setSelection(list.size() - 1);
        }
    };

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);
        getMessageList();
        message = findViewById(R.id.sms_send_content);
        mTitle = findViewById(R.id.sms_title);
        mSmsList = findViewById(R.id.sms_list);
        smsAdapter = new SmsAdapter(this.list);
        DisplayMetrics metric = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metric);
        int height = metric.heightPixels;
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mSmsList.getLayoutParams();
        layoutParams.height = height - getResources().getDimensionPixelOffset(R.dimen.dp35);
        mSmsList.setLayoutParams(layoutParams);
        setStatusBarColor();
        mSmsList.setAdapter(smsAdapter);
        mTitle.setOnClickListener(v -> finish());
        mSmsList.setSelection(this.list.size() - 1);
        findViewById(R.id.button).setOnClickListener(v -> {
            String msg = message.getText().toString();
            if (!TextUtils.isEmpty(msg)) {
                SmsReadDao.sendMessage(msg, SmsActivity.this, Constants.PERSON_PHONE);
                mHandler.sendEmptyMessageDelayed(1, 1500);
                message.setText("");
            }
        });
        final View decorView = this.getWindow().getDecorView();

        decorView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect rect = new Rect();
                decorView.getWindowVisibleDisplayFrame(rect);
                int mainInvisibleHeight = decorView.getRootView().getHeight() - rect.bottom;
                int screenHeight = decorView.getRootView().getHeight();
                if (mainInvisibleHeight > screenHeight / 4) {
                    if (!isSelect) {
                        isSelect = true;
                        mSmsList.setSelection(list.size() - 1);
                    }
                } else {
                    isSelect = false;
                }
            }
        });
        mSmsList.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect rect = new Rect();
                mSmsList.getWindowVisibleDisplayFrame(rect);
                int mainInvisibleHeight = decorView.getRootView().getHeight() - rect.bottom;
                int screenHeight = decorView.getRootView().getHeight();
                if (mainInvisibleHeight > screenHeight / 4) {
                    if (!isSelect) {
                        isSelect = true;
                        mSmsList.setSelection(list.size() - 1);
                    }
                } else {
                    isSelect = false;
                }
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SmsMessage event) {
        Log.d(TAG, "receive onMessageEvent");
        getMessageList();
        smsAdapter.notifyDataSetChanged();
        mSmsList.setSelection(list.size() - 1);
    }

    void setStatusBarColor() {
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.WHITE);
        }
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        ViewGroup mContentView = (ViewGroup) window.findViewById(Window.ID_ANDROID_CONTENT);
        View mChildView = mContentView.getChildAt(0);
        if (mChildView != null) {
            ViewCompat.setFitsSystemWindows(mChildView, false);
            ViewCompat.requestApplyInsets(mChildView);
        }

    }

    private void getMessageList() {
        list.clear();
        List<SmsBean> addList = SmsReadDao.obtainPhoneMessage(this, Constants.PERSON_PHONE);
        if (addList != null && addList.size() > 0) {
            list.addAll(addList);
        }
    }

}