package com.appcard.marketing.activity;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.appcard.marketing.receiver.AdminReceiver;
import com.appcard.marketing.utils.Constants;
import com.appcard.marketing.utils.UninstallBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class BaseActivity extends AppCompatActivity {
    public static String TAG = BaseActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        lockScreen();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            try {
                Window window = getWindow();
                WindowManager.LayoutParams lp = window.getAttributes();
                lp.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
                window.setAttributes(lp);
                final View decorView = window.getDecorView();
                decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            } catch (Exception e) {

            }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(UninstallBean event) {
        Log.d(TAG, "receive onMessageEvent");
        if (Constants.REMOVE_BROADCAST.equals(event.getBroadcast())) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Log.d(TAG, "" + dpm.isDeviceOwnerApp(getPackageName()));
                if (dpm.isDeviceOwnerApp(getPackageName())) {
                    dpm.clearDeviceOwnerApp(getPackageName());
                }
            }
        }
    }

    private DevicePolicyManager dpm;
    private boolean inKioskMode;
    private ComponentName deviceAdmin;

    private boolean doLockScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (dpm.isLockTaskPermitted(getPackageName())) {
                Log.i(TAG, "start lock screen");
                startLockTask();
                inKioskMode = true;
                Log.i(TAG, "lock screen success");
                return true;
            }
        }
        Log.w(TAG, "cannot lock screen");
        return false;
    }

    public void lockScreen() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (!inKioskMode) {
                    dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
                    deviceAdmin = new ComponentName(this, AdminReceiver.class);
                    Log.e(TAG, "isAdminActive: " + dpm.isAdminActive(deviceAdmin) + "\tisDeviceOwnerApp: " + dpm.isDeviceOwnerApp(getPackageName()));
                    if (dpm.isDeviceOwnerApp(getPackageName())) {
                        //  adb shell dpm set-device-owner com.appcard.marketing/.receiver.AdminReceiver
                        dpm.setLockTaskPackages(deviceAdmin,
                                new String[]{getPackageName(), "com.google.android.dialer", "com.android.dialer"});
                        doLockScreen();
                        Log.e(TAG, "setLockTaskPackages: ");
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
