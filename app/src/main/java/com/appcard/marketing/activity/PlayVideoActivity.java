package com.appcard.marketing.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.MediaController;

import androidx.annotation.RequiresApi;

import com.appcard.marketing.wigetview.CustomVideoView;
import com.appcard.marketing.R;
import com.appcard.marketing.utils.Constants;

public class PlayVideoActivity extends BaseActivity {
    private CustomVideoView video;
    private String action = "url";

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);
        video = findViewById(R.id.movie);
    }


    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        String url = Constants.VIDEO_URL;
        if (intent != null && !TextUtils.isEmpty(intent.getStringExtra(action))) {
            url = intent.getStringExtra(action);
        }
        Log.d(TAG, url);
        Uri parse = Uri.parse(url);
        video.setVideoURI(parse);
        MediaController controller = new MediaController(this);
        video.setMediaController(controller);
        video.requestFocus();
        video.start();
        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                setResult(0x129);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                finish();
            }
        });

    }

}