package com.appcard.marketing.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.appcard.marketing.R;
import com.appcard.marketing.utils.Constants;
import com.appcard.marketing.utils.Utils;
import com.bumptech.glide.Glide;
import com.google.android.material.imageview.ShapeableImageView;

public class MoreActivity extends BaseActivity implements View.OnClickListener {
    private ShapeableImageView video1;
    private ImageView pause1;
    private ShapeableImageView video2;
    private ImageView pause2;
    private ShapeableImageView video3;
    private ImageView pause3;
    private TextView mTitle;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more);
        mTitle = findViewById(R.id.title);
        video1 = findViewById(R.id.video1);
        pause1 = findViewById(R.id.video1);
        video2 = findViewById(R.id.video2);
        pause2 = findViewById(R.id.video2);
        video3 = findViewById(R.id.video3);
        pause3 = findViewById(R.id.video3);
        mTitle.setText(Utils.getInstance().stringFormat(Constants.MORE_TITLE));

    }

    @Override
    protected void onResume() {
        super.onResume();
        Glide.with(this).load(Constants.VIDEO_MORE_URL1).into(video1);
        Glide.with(this).load(Constants.VIDEO_MORE_URL2).into(video2);
        Glide.with(this).load(Constants.VIDEO_MORE_URL3).into(video3);

        video1.setOnClickListener(this);
        pause1.setOnClickListener(this);
        video2.setOnClickListener(this);
        pause2.setOnClickListener(this);
        video3.setOnClickListener(this);
        pause3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.video1:
            case R.id.pause1:
                playVideo(Constants.VIDEO_MORE_URL1);
                Utils.getInstance().sendNotifications(this, String.format(Constants.BUTTON, Constants.MORE1));
                break;
            case R.id.video2:
            case R.id.pause2:
                playVideo(Constants.VIDEO_MORE_URL2);
                Utils.getInstance().sendNotifications(this, String.format(Constants.BUTTON, Constants.MORE2));
                break;
            case R.id.video3:
            case R.id.pause3:
                playVideo(Constants.VIDEO_MORE_URL3);
                Utils.getInstance().sendNotifications(this, String.format(Constants.BUTTON, Constants.MORE3));
                break;
        }
    }

    public void playVideo(String url) {
        Intent intent = new Intent(this, PlayVideoActivity.class);
        intent.putExtra(Constants.ACTION, url);
        startActivity(intent);

    }

}